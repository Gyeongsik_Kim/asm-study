.386
.model flat, c
.stack 4096
.data
    message db "Hello, World!", 0Dh, 0Ah, 00h
 
.code
    extern _heap_init: proc
    extern _mtinit: proc
    extern _ioinit: proc
 
    extern exit: proc   
    extern printf: proc
 
    main proc
        ; _heap_init(1) 호출
        push 1
        call _heap_init
        add esp, 4 ;esp에 4가 추가되면 pop 과 동일한 기능
 
        ; _mtinit(1) 호출
        push 1
        call _mtinit
        add esp, 4
 
        ; _ioinit() 호출
        call _ioinit
 
        ; main() 함수 본문 시작
        ; C언어로 printf(message);의 한 줄은 아래와 같이 push, call, 스택정리(add esp, 4)의 3줄로 표현됩니다.
        push offset message
        call printf
        add esp, 4
 
        ; exit(0) 호출
        push 0
        call exit
        add esp, 4
 
        ; return 0
        xor eax, eax
        ret
    main endp
 
    end main
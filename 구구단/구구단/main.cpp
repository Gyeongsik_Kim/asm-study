#include <stdio.h>
void main(void){
	const char* format = "%d * %d = %d\n";
	int count = 1;
	__asm{
	OutLoop :
		MOV EBX, 0 //EBX 0으로 초기값 설정
		CMP count, 9
		JBE InLoop //9 이하이면 점프
		JMP exit //10 이상이 될경우 exit 

	InLoop :
		INC EBX //EBX 증가
		CMP EBX, 9 
		JA InOut //EBX가 9이상이면 outLoop로 돌아가기 위해서 inOut으로 점프
		JMP print 

	print:
		MOV EAX, count
		MUL EBX
		PUSH AL
		PUSH EBX
		PUSH count
		PUSH format
		CALL printf
		POP EAX
		POP EAX
		POP EAX
		POP EAX
		JMP InLoop //출력후 다시 InLoop로 점프
		

	InOut:
		INC count
		JMP OutLoop

	exit:
		JMP exit
	}
}
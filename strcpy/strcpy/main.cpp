#include <stdio.h>
#include <stdlib.h>

int __stdcall strlen(char* code) {
	__asm {
		MOV EBX, code
		MOV EAX, 0
		INLoop :
		INC EBX
			INC EAX
			CMP[EBX], 0x00
			JNE INLoop
	}
}

char* __stdcall strcpy(char* source, char* dest) {
	__asm {
		PUSH source
		CALL strlen
		MOV ESI, source
		MOV EDI, dest
		MOV ECX, EAX
		REP MOVS BYTE PTR[EDI], BYTE PTR[ESI]
		MOV BYTE PTR[EDI], 0x00
	}
}
int main(void) {
	char* test = "GyungDal";
	char* result = (char*)malloc(sizeof(test));
	strcpy(test, result);
	fputs(result, stdout);
	return 0;
}

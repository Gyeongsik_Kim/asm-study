#pragma once
int __stdcall strlen(char* code) {
	__asm {
		MOV EBX, code
		MOV EAX, 0
		INLoop :
		INC EBX
			INC EAX
			CMP[EBX], 0x00
			JNE INLoop
	}
}

int printf(char* output) {
	int count = strlen(output);
	__asm {
		MOV EAX, 4
		MOV EDI, 1
		MOV ESI, output
		MOV EDX, count
		SYSCALL
		MOV EAX, 1 
		XOR EDI, EDI
		SYSCALL
	}
}
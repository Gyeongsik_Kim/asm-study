#include <stdio.h>
#include <Windows.h>

int __stdcall asm_strlen(char* code) {
	__asm {
		MOV EBX, code
		MOV EAX, 0
		INLoop :
 		INC EBX
		INC EAX
		CMP [EBX], 0x00
		JNE INLoop
	}
}
